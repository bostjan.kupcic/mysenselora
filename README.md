# MySenseLora Project
Part of the MyRgbwwLed ecosystem (stairs led).

## Board:
LILYGO TTGO LORA32 868Mhz ESP32 LoRa OLED 0.96 Inch

## Project
PIR sensor via LoRa and MQTT.

## Brief
Two boards used in simple lora P2P (one transmitter other receiver) communication. Both boards use pir sensor. The transmitter sends the pir detection signal via lora to the receiver, the receiver currently sends the event to MQTT. Receiver also sends local pir event to MQTT.
